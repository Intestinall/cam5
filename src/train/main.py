# https://github.com/bonlime/keras-deeplab-v3-plus
import multiprocessing
from pathlib import Path

from src.app.model import Model
from src.app.utils import get_last_model_path  # noqa
from src.train.generator import get_train_and_validation_generators

WORKERS = multiprocessing.cpu_count()
TRAIN_PATH = Path("data")


if __name__ == "__main__":
    train_generator, val_generator = get_train_and_validation_generators(
        TRAIN_PATH, batch_size=4
    )
    # model = Model(backbone="mobilenetv2", classes=3)
    model = Model(custom_weights=get_last_model_path(Path("trained_models")))
    model.compile()

    model.fit_generator(
        train_generator,
        validation_data=val_generator,
        epochs=10000,
        use_multiprocessing=True,
        workers=WORKERS,
    )
