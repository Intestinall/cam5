import random
from pathlib import Path
from time import sleep

from src.app.model import Model
from src.app.utils import get_best_accuracy_model_path  # noqa
from src.app.utils import get_last_model_path  # noqa
from src.app.utils import get_last_session_model_path  # noqa
from src.train.image import Image

# https://github.com/bonlime/keras-deeplab-v3-plus
model_p = get_best_accuracy_model_path(Path("trained_models"))
# model_p = get_last_session_model_path(Path("trained_models"), offset=1, epoch=450)
model = Model(custom_weights=model_p)
TRAIN_PATH = Path("data")
images = [Image(p) for p in Path(TRAIN_PATH, "X").iterdir()]
random.shuffle(images)

for image in images[:5]:
    Image.plot(image.X)
    Image.plot(image.y)
    prediction, _ = model.predict(image.X, 1, mask_only=True)
    Image.plot(prediction)
    sleep(1)

sleep(3600)
