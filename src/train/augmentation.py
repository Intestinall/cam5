from albumentations import (
    CLAHE,
    Blur,
    ChannelShuffle,
    Compose,
    Flip,
    GaussNoise,
    HueSaturationValue,
    IAAEmboss,
    IAASharpen,
    ISONoise,
    MedianBlur,
    MotionBlur,
    MultiplicativeNoise,
    OneOf,
    OpticalDistortion,
    RandomBrightnessContrast,
    RandomGamma,
    RGBShift,
    ShiftScaleRotate,
)


def get_data_aug():
    return Compose(
        [
            Flip(p=1),
            ShiftScaleRotate(shift_limit=0.5, scale_limit=0.5, rotate_limit=200, p=1),
            OpticalDistortion(distort_limit=0.4, shift_limit=0.4, p=1),
            OneOf([ISONoise(), GaussNoise(), MultiplicativeNoise()], p=0.8),
            OneOf([MotionBlur(), MedianBlur(), Blur()], p=0.8),
            OneOf([CLAHE(), IAASharpen(), IAAEmboss()]),
            OneOf(
                [
                    RandomBrightnessContrast(brightness_limit=0.3, contrast_limit=0.3),
                    RandomGamma(gamma_limit=(50, 150)),
                ],
                p=1,
            ),
            OneOf(
                [
                    HueSaturationValue(
                        hue_shift_limit=40, sat_shift_limit=40, val_shift_limit=40
                    ),
                    ChannelShuffle(),
                    RGBShift(),
                ]
            ),
        ]
    )
