from pathlib import Path

import numpy as np
from PIL import Image as PIL_IMAGE

from src.app.colors import COLORS


class Image:
    def __init__(self, X_path: Path, size: int = 512) -> None:
        self.X_path = X_path
        self.y_path = self._get_y_path()
        self._size = (size, size)
        self._image_main_class = self._get_image_main_class()

    def _get_image_main_class(self) -> int:
        y_ = self.y
        for i, colors in enumerate(COLORS[1:], start=1):
            if np.all(y_ == colors, axis=-1).any():
                return i
        raise ValueError(f"No known class found in {self.y_path}")

    def _get_y_path(self) -> Path:
        return Path(self.X_path.parents[1], "Y", self._x_to_y_file())

    def _x_to_y_file(self) -> str:
        return (
            self.X_path.with_name(f"{self.X_path.with_suffix('').name}_color_mask")
            .with_suffix(".png")
            .name
        )

    def _image_to_np_array(self, path: Path) -> np.ndarray:
        return np.array(
            PIL_IMAGE.open(path).convert("RGB").resize(self._size, PIL_IMAGE.NEAREST)
        ).astype(np.uint8)

    @property
    def X(self) -> np.array:
        """
        In case of grayscale images, the following piece of code might be required :
        if len(img.shape) == 2:
            img = np.stack((img,) * 3, axis=-1)
        """
        return self._image_to_np_array(self.X_path)

    @property
    def y(self) -> np.array:
        return self._image_to_np_array(self.y_path)

    def rgb_to_labels(self, rgb: np.array) -> int:
        """
        Because our dataset contain only image with two classes in it :
        background and the object class.
        """
        return 0 if np.sum(rgb) == 0 else self._image_main_class

    def y_to_mask(self, y: np.array) -> np.ndarray:
        return np.apply_along_axis(self.rgb_to_labels, -1, y)

    @staticmethod
    def plot(img: np.ndarray) -> None:
        PIL_IMAGE.fromarray(img, "RGB").show()
