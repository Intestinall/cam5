import random
from pathlib import Path
from typing import List, Tuple

import numpy as np
from tensorflow.python.keras.utils.data_utils import Sequence

from src.app.colors import COLORS  # noqa
from src.train.augmentation import get_data_aug
from src.train.image import Image


class ImageGenerator(Sequence):
    def __init__(self, image_list: List[Image], batch_size: int) -> None:
        self._image_list = image_list
        self._data_aug = get_data_aug()
        self._batch_size = batch_size

    def __len__(self) -> int:
        return len(self._image_list)

    def __getitem__(self, _idx: int) -> Tuple[np.ndarray, np.ndarray]:
        images: List[Image] = random.sample(self._image_list, k=self._batch_size)
        X_s, y_s = [], []

        for image in images:
            seed = random.getrandbits(32)
            X, y = self._augment_X_y(image, seed)
            X_s.append(X)
            y_s.append(y)
            # Image.plot(X)
            # Image.plot(COLORS[y])
        return np.asarray(X_s), np.asarray(y_s)

    def _augment_X_y(self, image: Image, seed: int) -> Tuple[np.ndarray, np.ndarray]:
        aug = self._data_aug(image=image.X, mask=image.y_to_mask(image.y))
        return aug["image"], aug["mask"]


def get_train_and_validation_generators(
    train_path: Path, batch_size: int, training_part: float = 0.8,
) -> Tuple[Sequence, Sequence]:
    image_list = [Image(p) for p in Path(train_path, "X").iterdir()]
    random.shuffle(image_list)
    train_size = int(len(image_list) * training_part)
    train, validation = image_list[:train_size], image_list[train_size:]
    return (
        ImageGenerator(train, batch_size=batch_size),
        ImageGenerator(validation, batch_size=1),
    )
