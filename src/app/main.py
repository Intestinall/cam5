import os
from pathlib import Path
from typing import Iterator, Optional, Tuple

import imutils
import numpy as np
from kivy.app import App
from kivy.clock import Clock, ClockEvent
from kivy.core.window import Window
from kivy.graphics.context_instructions import Color
from kivy.graphics.texture import Texture
from kivy.graphics.vertex_instructions import Rectangle
from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image as KvImage
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import Screen, ScreenManager  # noqa
from PIL import Image as PillowImage

from src.app.aws import push_file_on_s3
from src.app.colors import GLASSES_COLORS, KEYS_COLORS
from src.app.model import Model
from src.app.utils import get_best_accuracy_model_path
from src.app.video import Video

DEEPLAB_MODEL = Model(
    custom_weights=get_best_accuracy_model_path(Path("trained_models"))
)
VIDEO: Optional[Video] = None
LABEL: Optional[int] = None
CURRENT_COLORS: Optional[np.ndarray] = None


class KivyCamera(KvImage):
    def __init__(self, **kwargs) -> None:
        super(KivyCamera, self).__init__(**kwargs)
        self.video: Optional[Video] = None
        self.video_frames: Optional[Iterator[bool, np.ndarray]] = None
        self.event: Optional[ClockEvent] = None
        self.texture: Texture
        self.focus: bool = False

    def start(self, video, fps: int = 30) -> None:
        self.video = video
        self.video_frames = video.frames()
        self.event = Clock.schedule_interval(self.update, 1.0 / fps)

    def stop(self) -> None:
        self.event.cancel()
        self.video = None
        self.video_frames = None
        self.event = None

    def update(self, _delay_time: float) -> None:
        """_delay_time: time in seconds since the last call to this function."""
        ret: bool
        frame: np.ndarray
        ret, frame = next(self.video_frames)
        if ret:
            labels: np.ndarray
            found: bool
            labels, found = DEEPLAB_MODEL.predict(
                frame, LABEL, CURRENT_COLORS, mask_only=self.focus
            )

            w: int
            h: int
            w, h = labels.shape[1], labels.shape[0]

            if not self.texture or self.texture.width != w or self.texture.height != h:
                self.texture = Texture.create(size=(w, h))
                self.texture.flip_horizontal()

            self.bg_color((1.0, 0.0, 0.0) if found else (0.04, 0.04, 0.21))
            self.texture.blit_buffer(labels.tobytes())
            self.canvas.ask_update()

    def screenshot(self) -> np.ndarray:
        frame: np.ndarray
        _, frame = next(self.video_frames)
        return frame

    def bg_color(self, color: Tuple[float, float, float]) -> None:
        with self.canvas.before:
            Color(*color)
            Rectangle(pos=self.pos, size=self.size)

    def focusMode(self) -> bool:
        self.focus = not self.focus
        return self.focus


class PostUploadPopup(FloatLayout):
    pass


class MenuScreen(Screen):
    def labels(self, n: int) -> None:
        global LABEL, CURRENT_COLORS
        LABEL = n
        if n == 2:
            CURRENT_COLORS = KEYS_COLORS
        else:
            CURRENT_COLORS = GLASSES_COLORS


class PopupTempImage:
    IMAGE_NAME = "__popup_image__.png"

    def __init__(self, frame: np.ndarray) -> None:
        self.frame = frame

    def _get_texture(self) -> Texture:
        w: int
        h: int
        w, h = self.frame.shape[1], self.frame.shape[0]
        texture = Texture.create(size=(w, h))
        texture.flip_horizontal()
        return texture

    def __enter__(self):
        PillowImage.fromarray(imutils.rotate(self.frame, 180)).save(
            PopupTempImage.IMAGE_NAME
        )
        image = KvImage(
            source=PopupTempImage.IMAGE_NAME,
            allow_stretch=True,
            texture=self._get_texture(),
        )
        image.reload()
        return image

    def __exit__(self, _type, _value, _traceback):
        try:
            os.remove(PopupTempImage.IMAGE_NAME)
        except FileNotFoundError:
            pass


class VideoScreen(Screen):
    POPUP_KWARGS = dict(
        size_hint=(None, None),
        size=(400, 600),
        auto_dismiss=True,
        title_size=30,
        title_align="center",
    )
    focusBtn = ObjectProperty(None)

    def on_enter(self, *args) -> None:
        global VIDEO
        VIDEO = Video()
        self.ids.cam.start(VIDEO)

    def doexit(self) -> None:
        global VIDEO
        self.ids.cam.stop()
        VIDEO = None

    def capture(self, _: Button) -> None:
        self.popup.dismiss()
        self.popup = None
        push_file_on_s3(self.frame)
        self.frame = None
        self.show_post_upload_popup()

    def focus(self) -> None:
        focusMode = self.ids.cam.focusMode()
        if focusMode:
            self.focusBtn.text = "Switch to Normal mode"
            self.focusBtn.background_color = (1, 1, 1, 1)
        else:
            self.focusBtn.text = "Switch to Focus mode"
            self.focusBtn.background_color = (0.7, 0, 0, 1)

    def show_post_upload_popup(self) -> None:
        show = PostUploadPopup()  # Create a new instance of the P class
        popupWindow = Popup(
            title="Confirmation", content=show, **VideoScreen.POPUP_KWARGS
        )
        popupWindow.open()

    def show_popup(self) -> None:
        self.frame = self.ids.cam.screenshot()

        with PopupTempImage(self.frame) as temp_image:
            box = BoxLayout(orientation="vertical", padding=10)
            box.add_widget(temp_image)
            self.popup = Popup(
                title="Are you sure you want to upload this image ?",
                content=box,
                **VideoScreen.POPUP_KWARGS
            )
            box2 = BoxLayout(
                orientation="horizontal",
                cols=2,
                size_hint_y=0.2,
                size_hint_x=1,
                pos_hint={"center_x": 0.65},
                spacing=50,
            )
            box2.add_widget(
                Button(
                    text="OK",
                    on_press=self.capture,
                    size_hint=(None, None),
                    background_color=(0, 0.8, 0, 1),
                )
            )
            box2.add_widget(
                Button(
                    text="CANCEL",
                    on_press=self.popup.dismiss,
                    size_hint=(None, None),
                    background_color=(0.8, 0, 0, 1),
                )
            )
            box.add_widget(box2)
            self.popup.open()


class camApp(App):
    def build(self):
        Window.size = (400, 800)
        return Builder.load_file("cam.kv")


camApp().run()
