from dataclasses import dataclass
from typing import Iterator, Optional, Tuple

import cv2
import imutils
import numpy as np


@dataclass
class Video:
    ROTATION_ANGLE = 90

    def __init__(
        self, capture_device: Optional[cv2.VideoCapture] = None,
    ):
        self.capture_device = capture_device or cv2.VideoCapture(0)

    def frames(
        self, rotation_angle: int = ROTATION_ANGLE, cv2_color: int = cv2.COLOR_BGR2RGB
    ) -> Iterator[Tuple[bool, np.ndarray]]:
        while True:
            ret: bool
            frame: np.ndarray
            ret, frame = self.capture_device.read()
            if not ret:
                yield ret, np.ndarray((0,))
            else:
                yield ret, self.process_frame(frame, rotation_angle, cv2_color)

    @staticmethod
    def process_frame(
        frame: np.ndarray, rotation_angle: int, cv2_color: int = cv2.COLOR_BGR2RGB
    ) -> np.ndarray:
        return cv2.cvtColor(imutils.rotate(frame, rotation_angle), cv2_color).astype(
            np.uint8
        )
