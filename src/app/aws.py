import os
import uuid

import boto3
import cv2
import numpy as np

from src.app.video import Video

BUCKET = "cam5-cam5-cam5"
S3 = boto3.resource(
    "s3",
    aws_access_key_id=os.environ["AWS_ACCESS_KEY_ID"],
    aws_secret_access_key=os.environ["AWS_SECRET_ACCESS_KEY"],
)


def push_file_on_s3(frame: np.ndarray) -> None:
    image = cv2.imencode(".png", Video.process_frame(frame, 180, cv2.COLOR_RGB2BGR))[1]
    key = str(uuid.uuid4())
    S3.Bucket(BUCKET).put_object(
        Key=f"{key}.png", Body=image.tostring(), ContentType="image/PNG"
    )
