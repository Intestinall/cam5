import numpy as np

DETECTION_THRESHOLD: float = 0.01


def colorize(image: np.ndarray, labels: np.ndarray, colors: np.ndarray) -> np.ndarray:
    return ((colorize_mask(labels, colors) + image.astype(np.float64)) / 2).astype(
        np.uint8
    )


def colorize_mask(labels: np.ndarray, colors: np.ndarray) -> np.ndarray:
    return colors[labels]


def detect_object(
    labels: np.ndarray, class_to_search: int, threshold: float = DETECTION_THRESHOLD
) -> bool:
    limit = int(labels.shape[0] * labels.shape[1] * threshold)
    return np.count_nonzero(labels == class_to_search) > limit
