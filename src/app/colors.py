import numpy as np

COLORS = np.asarray(
    ((0, 0, 0), (255, 0, 0), (0, 0, 255))  # background  # glasses  # keys
).astype(np.uint8)

GLASSES_COLORS = np.asarray(((0, 0, 0), (255, 0, 0), (0, 0, 0))).astype(np.uint8)

KEYS_COLORS = np.asarray(((0, 0, 0), (0, 0, 0), (255, 0, 0))).astype(np.uint8)
