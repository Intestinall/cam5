from time import time


class Fps:
    def __init__(self, threshold: int = 10, display_fps: bool = True) -> None:
        self.start_time: float = time()
        self.frame_count: int = 0
        self.threshold: int = threshold
        self.display_fps: bool = display_fps

    def update(self):
        self.frame_count += 1
        if self.frame_count >= self.threshold:
            stop_time: float = time()
            self._display_fps(stop_time)
            self.start_time = stop_time
            self.frame_count = 0

    def _display_fps(self, stop_time: float) -> None:
        if self.display_fps:
            fps: float = self.frame_count / (stop_time - self.start_time)
            print(f"{fps:.1f} fps")
