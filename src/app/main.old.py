from pathlib import Path

import cv2
import numpy as np

from src.app.fps import Fps
from src.app.model import Model

# https://github.com/bonlime/keras-deeplab-v3-plus
from src.app.utils import get_last_model_path
from src.app.video import Video

DEEPLAB_MODEL = Model(custom_weights=get_last_model_path(Path("trained_models")))
STATS = Fps()
VIDEO = Video()

CLASS_TO_SEARCH = 60

for frame in (frame for ret, frame in VIDEO.frames(rotation_angle=-90) if ret):
    labels: np.ndarray
    object_found: bool
    labels, object_found = DEEPLAB_MODEL.predict(frame, CLASS_TO_SEARCH)

    STATS.update()
    cv2.imshow("Cam5", labels)

    if cv2.waitKey(1) & 0xFF == ord("q"):
        exit()
