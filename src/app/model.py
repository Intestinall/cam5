from datetime import datetime
from pathlib import Path
from typing import Tuple

import cv2
import numpy as np
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard
from tensorflow.keras.losses import sparse_categorical_crossentropy
from tensorflow.keras.optimizers import Adam
from tensorflow.python.keras.utils.data_utils import Sequence

from deeplabv3.model import Deeplabv3
from src.app.colors import COLORS
from src.app.image_processing import colorize, colorize_mask, detect_object


class Model:
    CLASSES = 3
    ACTIVATION = "softmax"
    TRAINED_IMAGE_WIDTH: int = 512
    _TID_TUPLE = (TRAINED_IMAGE_WIDTH, TRAINED_IMAGE_WIDTH)

    def __init__(self, classes=CLASSES, activation=ACTIVATION, *args, **kwargs) -> None:
        self.inner = Deeplabv3(classes=classes, activation=activation, *args, **kwargs)
        self.session_now = datetime.now().strftime("session_%Y_%m_%d_%H_%M_%S")

    def compile(self, optimizer=None, loss=None, lr=1e-5) -> None:
        optimizer = optimizer or Adam(lr=lr)
        loss = loss or sparse_categorical_crossentropy
        self.inner.compile(optimizer=optimizer, loss=loss, metrics=["accuracy"])

    def predict(
        self,
        image: np.ndarray,
        class_to_search: int,
        colors: np.ndarray = COLORS,
        mask_only: bool = False,
    ) -> Tuple[np.ndarray, bool]:
        image = cv2.resize(image, Model._TID_TUPLE)
        labels = self._get_labels(image)
        object_found = detect_object(labels, class_to_search)
        image_to_display = (
            colorize_mask(labels, colors)
            if mask_only
            else colorize(image, labels, colors)
        )
        return image_to_display, object_found

    def _get_labels(self, image: np.ndarray) -> np.ndarray:
        res = self.inner.predict(np.expand_dims(image, 0))
        return np.argmax(res.squeeze(), -1)

    def fit_generator(
        self, generator: Sequence, save_period: int = 1, **kwargs
    ) -> None:
        mc = self._get_model_checkpoint_callback(save_period)
        tb = self._get_tensorboard_callback()
        self.inner.fit_generator(generator, **kwargs, callbacks=[mc, tb])

    def _get_model_checkpoint_callback(self, save_period: int) -> ModelCheckpoint:
        format_str = (
            "cam5_mobilenetv2_epoch_{epoch:04d}_"
            + "_".join(
                f"{x}_{{{x}:010.8f}}"
                for x in ("loss", "accuracy", "val_loss", "val_accuracy")
            )
            + ".h5"
        )
        # TODO : period is deprecated, use save_freq instead.
        saves_path = Path("trained_models", self.session_now).resolve()
        saves_path.mkdir(parents=True, exist_ok=True)
        return ModelCheckpoint(
            str(Path(saves_path, format_str).resolve()), period=save_period,
        )

    def _get_tensorboard_callback(self) -> TensorBoard:
        logs_path = Path("logs", self.session_now)
        logs_path.mkdir(parents=True, exist_ok=True)
        return TensorBoard(log_dir=logs_path, histogram_freq=1)
