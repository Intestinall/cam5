from pathlib import Path


def get_last_session_path(models_path: Path, offset: int) -> Path:
    return sorted(models_path.iterdir(), key=lambda x: x.name)[-(1 + offset)]


def get_last_model_path(models_path: Path, offset: int = 0) -> Path:
    last_session_path = get_last_session_path(models_path, offset)
    return sorted(last_session_path.iterdir(), key=lambda x: x.name)[-1]


def get_last_session_model_path(models_path: Path, epoch: int, offset: int = 0) -> Path:
    last_session_path = get_last_session_path(models_path, offset)
    return next(
        p for p in last_session_path.iterdir() if f"epoch_{epoch:04d}" in p.name
    )


def get_best_accuracy_model_path(models_path: Path, offset: int = 0) -> Path:
    last_session_path = get_last_session_path(models_path, offset)
    f = lambda x: x.name.split("val_accuracy_", 1)[1][: -len(".h5")]  # noqa
    return max(last_session_path.iterdir(), key=f)
